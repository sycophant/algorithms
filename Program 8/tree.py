##########################
#Program 8: Tree Traversal
#Professor: Carol Browning
#Authors: B. Zimmerman, A.Hasnat
##########################

###############
#PART A
###############

#Returns the inorder traversal of a tree
def inorder(tree):
  traversal = []
  if tree[0]:
    if tree[1]:
      traversal += inorder(tree[1])
    traversal.append(tree[0])
    if tree[2]:
      traversal += inorder(tree[2])
  return traversal

#Returns the preorder traversal of a tree
def preorder(tree):
  traversal = []
  if tree[0]:
    traversal.append(tree[0])
    if tree[1]:
      traversal += preorder(tree[1])
    if tree[2]:
      traversal += preorder(tree[2])
  return traversal

#Returns the postorder traversal of a tree
def postorder(tree):
  traversal = []
  if tree[0]:
    if tree[1]:
      traversal += postorder(tree[1])
    if tree[2]:
      traversal += postorder(tree[2])
    traversal.append(tree[0])
  return traversal

###############
#PART B
###############

#Returns the number of leaves in a tree
def leafCount(tree):
  count = 0
  if tree[1]:
    count += leafCount(tree[1])
  if tree[2]:
    count += leafCount(tree[2])
  if (not tree[1] and not tree[2]):
    count += 1
  return count

#Test suite
def testCase(testNumber,function,tree,expectedAnswer):
  if expectedAnswer==function(tree):
    print("Test",testNumber,"passed.")
  else:
    print("Test",testNumber,"failed.")
    
def test1():
  f=["f",[],[]]
  c=["c",f,[]]
  e=["e",[],[]]
  g=["g",[],[]]
  d=["d",[],g]
  b=["b",d,e]
  root=["a",b,c]
  testCase(1,inorder,root,['d', 'g', 'b', 'e', 'a', 'f', 'c'])
  testCase(2,preorder,root,['a', 'b', 'd', 'g', 'e', 'c', 'f'])
  testCase(3,postorder,root,['g', 'd', 'e', 'b', 'f', 'c', 'a'])
  testCase(4,inorder,c,['f','c'])
  testCase(5,preorder,e,['e'])
  testCase(6,leafCount,root,3)
  testCase(7,leafCount,e,1)

###############
#PART C
###############

def test2():
  d=["d",[],[]]
  e=["e",[],[]]
  f=["f",[],[]]
  g=["g",[],[]]
  b=["b",d,e]
  c=["c",f,g]
  root=["a",b,c]
  testCase(8,preorder,root,['a','b','d','e','c','f','g'])
  testCase(9,leafCount,root,4)


#####################
#Name : Arif Hasnat
#Student ID: 482131
#Class: Algortihms
#Date: 05/09/19
#Professor: Browning
#####################

#####################
#Part A
#####################

#test function as given
def testCaseCeleb(testNumber,adjMatrix,expectedResult):
	actualResult=findCeleb(adjMatrix)
	if actualResult==expectedResult:
		print("Test", testNumber,"passed.")
	else:
		print("Test", testNumber,"failed.  Expected ",expectedResult,"but found",actualResult)


#test function as given
def test():					
	testCaseCeleb(1,[[0,1,1],[0,0,0],[1,1,0]],1) 
	testCaseCeleb(2,[[0,0,1],[0,0,0],[1,1,0]],-1) 
	testCaseCeleb(3,[[0,1,1],[0,0,1],[1,1,0]],-1) 
	testCaseCeleb(4,[[0,1,0],[0,0,1],[1,0,0]],-1)
	testCaseCeleb(6,[[0,1,0,0],[0,0,1,0],[1,0,0,0],[1,0,0,0]],-1)
	testCaseCeleb(6,[[0,0,1,0],[1,0,0,0],[1,0,0,0],[0,1,0,0]],-1)

#function to find the sink aka celebrity
def findCeleb(adjMatrix):
	a = 0	#setting up candidate for comparison while iterating through the matrix
	n = len(adjMatrix)	#setting up size of matrix n
	#for loop to iterate through matrix looking for probable sink
	for i in range(1, n):
		if (adjMatrix[a][i]):
			a = i
	#series of bundled guards going off conditions of sink aka zero row, and n-1 column
	for i in range(0, n):
		if i != a and (adjMatrix[i][a] == 0 or adjMatrix[a][i] == 1):
			return -1	#returns a negative if no sink is found or sink not possible
	return a	#returns final candidate for sink

#proof of passing tests
# Python 3.7.4 (default, Jul  9 2019, 00:06:43)
# [GCC 6.3.0 20170516] on linux
#  test()
# Test 1 passed.
# Test 2 passed.
# Test 3 passed.
# Test 4 passed.
# Test 6 passed.
# Test 6 passed.

#####################
#Part B
#####################

#1 6 entries being the worst case scenario. Looking for a row of zeros, so first element of rows may be checked, which is at max 3.
#	Then column must be checked for n-1 scenario, again being 3 entries at max.
#2 2n-1 entries being the worst case scenario, which is running through rows and columns and finding an
#	entry that violates n-1, lending to complexity of O(n)
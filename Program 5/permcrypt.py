#####################
#Name : Arif Hasnat
#Student ID: 482131
#Class: Algortihms
#Date: 05/09/19
#Professor: Browning
#####################

#####################
#STRATEGY
#####################

# The idea was to understand how crptarithm solving worked, use python's default
# libraries and its functions to handle the majority of the process, so I solved 
# cryptarithms by hand and then searched around for python functions that would let me 
# do exactly what I did to generally handle a cryptarithm. One approach is up a 
# permutation table using dict, and combined permutations of digits and letters
# using zip. Another one is the maketrans function, for which it takes string arrays
# and matches charas from one string to charas of another string.The resulted table 
# is iterated through with addends via a for loop, and then summed up by the sum
# function.

# https://see.stanford.edu/materials/icspacs106b/H19-RecBacktrackExamples.pdf

#####################
#CODE
#####################

from itertools import chain, permutations

# Test cases for the algorithm. Added two that fit, rest is provided code.
def test():
    testCase(1,['a','b','cd'],[1,4,6,8],["6+8=14","8+6=14"])
    testCase(2,['q','r','st'],[5,2,1,7],["5+7=12","7+5=12"])
    testCase(3,['xyz','zx','xzw'],[4,7,0,3],["304+43=347","403+34=437"])
    testCase(4,['ac','b','ad'],[1,6,8,9],["61+8=69","68+1=69"])
    testCase(5,['lo','n','ml'],[5,6,7,8],["57+8=65","58+7=65"])

def testCase(testNumber,puzzle,digits,expectedList):
    actualResult = solve(puzzle,digits)
    if actualResult in expectedList: print ("Test",testNumber,"passed.")
    else: print ("Test",testNumber,"failed.  Expected one of: ",expectedList," Actual: ",actualResult)

#solve method that takes in a string array of words, and an int array of possible digits.
def solve(puzzle, digits):
    addends = [puzzle[i] for i in (0,1)]    #breaking up the strings into relevant portions for matching up with permutation table
    result = puzzle[2]  #same as above.
    numbers = ''.join(str(e) for e in digits)   #formatting to concatenated string to be manipulable by functions.
    letters = ''.join(set(chain(result, *addends))) #same as above, assisted by chain function.
    for perm in permutations(numbers, len(letters)):    #permutation function provides arrays of all possible digit combinations
        decipher_table = str.maketrans(letters, ''.join(perm))  #maketrans function zips letters and digit combinations to create a table
        def decipher(s):
            return s.translate(decipher_table)  #translate function deciphers the table using already matched up format
        deciphered_sum = sum(int(decipher(addend)) for addend in addends)   #check for additive cryptarithm in all permutations via for loop.
        if deciphered_sum == int(decipher(result)):
            def format(s):
                return f"{decipher(s)}"
            return ("+".join(map(format, addends))+"="+format(result))    #formatting the result.
#Thomas Tewes

assignmentList = ([0,1,2,3],[0,1,3,2],[0,3,1,2],[3,0,1,2],
                  [3,0,2,1],[0,3,2,1],[0,2,3,1],[0,2,1,3],
                  [2,0,1,3],[2,0,3,1],[2,3,0,1],[3,2,0,1],
                  [3,2,1,0],[2,3,1,0],[2,1,3,0],[2,1,0,3],
                  [1,2,0,3],[1,2,3,0],[1,3,2,0],[3,1,2,0],
                  [3,1,0,2],[1,3,0,2],[1,0,3,2],[1,0,2,3])

#Input: The puzzle, the digitList, and an orderList that decides what
#        numbers are assigned to what letters.
#Output: A list that has each letter, and what number it is assigned to.
def findAssignment(puzzle, digitList, orderList):
    charList = [];
    assignmentNum = [];
    assignment = [];
    for word in puzzle:
        for letter in word:
            if letter not in charList:
                charList = charList + [letter];
    for i in orderList:
        assignmentNum = assignmentNum + [digitList[i]];
    for j in range(4):
        assignment = assignment + [(charList[j], assignmentNum[j])];
    return assignment;

def getNum(puzzle, assignment):
    num = [];
    assignmentOne = assignment[0];
    assignmentTwo = assignment[1];
    assignmentThree = assignment[2];
    assignmentFour = assignment[3];
    for word in puzzle:
        ans = "";
        for letter in word:
            if (letter == assignmentOne[0]):
                ans = ans + str(assignmentOne[1]);
            elif (letter == assignmentTwo[0]):
                ans = ans + str(assignmentTwo[1]);
            elif (letter == assignmentThree[0]):
                ans = ans + str(assignmentThree[1]);
            elif (letter == assignmentFour[0]):
                ans = ans + str(assignmentFour[1]);
        num = num + [ans];
    return num;

def validAssignment(numOrder):
    numOne = int(numOrder[0]);
    numTwo = int(numOrder[1]);
    numThree = int(numOrder[2]);
    if ((numOne + numTwo) == numThree):
        return 1;
    return 0;

#Input: A Cryptarithmatic Puzzle and a corresponding digitList.
#Output: The possible outcomes of the puzzle with respect to the corresponding digitList.
#        If there are none, it returns "".
def solve(puzzle, digitList):
    for list in assignmentList:
        assignment = findAssignment(puzzle, digitList, list);
        numList = getNum(puzzle, assignment);
        numOne = int(numList[0]);
        numTwo = int(numList[1]);
        numThree = int(numList[2]);
        if (validAssignment(numList) == 1):
            return (str(numOne) + "+" + str(numTwo) + "=" + str(numThree));
    return "";

#The testCase function takes a test identifier,
#inputs for solve, and the expected result.
#It then calls solve and prints a message showing the test result.

def testCase(testNumber,puzzle,digits,expectedList):
    actualResult = solve(puzzle,digits)
    if actualResult in expectedList:
        print ("Test",testNumber,"passed.")
    else:
        print ("Test",testNumber,"failed.  Expected one of: ",expectedList," Actual: ",actualResult)


#This function runs the tests for solve.
#When grading your code, I will run several additional tests.
#I advise you to create additional test cases to check your code.

def test():
    testCase(1,['a','b','cd'],[1,4,6,8],["6+8=14","8+6=14"])
    testCase(2,['q','r','st'],[5,2,1,7],["5+7=12","7+5=12"])
    testCase(3,['xyz','zx','xzw'],[4,7,0,3],["304+43=347","403+34=437"])
#YOUR ADDITIONAL TESTS:
    testCase(4,['ac','b','ad'],[1,6,8,9],["61+8=69","68+1=69"])
    testCase(5,['lo','n','ml'],[5,6,7,8],["57+8=65","58+7=65"])

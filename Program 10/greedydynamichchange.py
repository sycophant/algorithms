#File Name: Program 10
#Authors: Arif, Ben and Jeremy
#Nov. 18, 2019
#CSCI262
#Purpose: Greedy vs. Dynamic change

#####################
#PART B
#####################

def testCase(testNumber,function,amount,denominations,expectedAnswer):
  if function(amount,denominations)==expectedAnswer:
    print("Test",testNumber,"passed:",amount, denominations,expectedAnswer)
  else:
    print("Test",testNumber,"failed.")

def test():
  testCase(1,greedyChange,48,[1,5,10,25],[25,10,10,1,1,1])
  testCase(2,dynamicChange,48,[1,5,10,25],[25,10,10,1,1,1]) 

  testCase(3,greedyChange,30,[1,5,10,25],[25,5])
  testCase(4,dynamicChange,30,[1,5,10,25],[25,5])

  testCase(5,greedyChange,30,[1,10,25],[25,1,1,1,1,1])
  testCase(6,dynamicChange,30,[1,10,25],[10,10,10])
  
  testCase(7,greedyChange,6,[1,3,4],[4,1,1])
  testCase(8,dynamicChange,6,[1,3,4],[3,3])

def greedyChange(amount,denomList):
  output=[]
  if amount==0:
    return output
  for i in range(len(denomList)-1,-1,-1):
    if denomList[i]<=amount:
      output.append(denomList[i])
      output+=greedyChange(amount-denomList[i],denomList)
      break
  #print(output)
  return output

#import random
# def dynamicChange(amount, denomList):
#   A = [[]]
#   F = []
#   F.append(0)
#   for i in range(1, amount):
#     temp = random.getrandbits(128)
#     j = 1
#     while (j<=len(denomList)) and (i>=denomList[j-1]):
#       #print('in while')
#       temp=min(F[i-denomList[j-1]],temp)
#       j += 1
#     F.append(temp + 1)
#   A.append(F)
#   print(A)
#   return A

#passes tests
def dynamicChange(amount, denomList):
  table = [0 for x in range(amount + 1)]
  table[0] = []
  for i in range(1, amount + 1):
    for coin in denomList:
      if (coin > i) : continue
      elif (not table[i]) or ((len(table[i - coin])) + 1) < (len(table[i])):
        if table[i - coin] != 0:
          table[i] = table[i - coin][:]
          table[i].append(coin)
  print (table)
  return table[len(table)-1]

test()
def test():
  testPair(1,'eat','tea',True)
  testPair(2,'Clint Eastwood','Old West Action',True)
  testPair(3,'arranged','deranged',False)

def testPair(testNumber,word1,word2,expected):
  if anagram(word1,word2)==expected:
    result = "passed"
  else:
    result = "failed"
  print("Test "+str(testNumber)+"  "+result+":"+ word1+" "+word2+" "+str(expected))

def anagram(word1,word2):
  word1 = word1.lower()
  word2 = word2.lower()
  count=0
  count1=0

  for i in range(97,123):
    if chr(i) in word1 and chr(i) in word2:
      count1+=1
      if word1.count(chr(i)) == word2.count(chr(i)):
        count+=1

  if count == count1:
    return True
  else:
      return False
############################
#Horspool Algorithm
#Professor: Carol Browning
#Authors: B. High, A. Hasnat
############################

############################
#PART B
############################

def shiftTable(pattern):
  table = {}
  for i in range(len(pattern)-1):
    char = pattern[i]
    j = i+1
    while char != pattern[j] and j < (len(pattern)-1):
      j += 1
    table[char] = j-i
  return table 

def shiftTestCase(testNum,pattern,answer):
  result = shiftTable(pattern)
  if result == answer:
    print('Test',testNum,'passed')
  else:
    print('Test',testNum,'failed.  Expected',answer,'but returned',result)

def testShiftTable():
	shiftTestCase(1,"barber",{'a': 4, 'b': 2, 'e': 1, 'r': 3})
	shiftTestCase(2,"leader",{'l': 5, 'e': 1, 'a': 3, 'd': 2})
	shiftTestCase(3,"reorder",{'r': 3, 'e': 1, 'o': 4, 'd': 2})
	shiftTestCase(4,"TCCTATTCTT",{'T': 1, 'C': 2, 'A': 5})

############################
#PART C, D
############################

def testCaseH(testNumber, string, text,expectedResult):
	actualResult = HorspoolMatching(string,text)
	if actualResult == expectedResult:
		print("Test",testNumber,"passed")
	else:
		print ("Test",testNumber,"failed.  Expected",expectedResult, "but found",actualResult)

def testHorspool():
	#testCaseH(1, "barber","jim saw me in a barbershop",16)
	testCaseH(1, "barber","jim saw me in a barbershop",[16,12])
	#testCaseH(2, "were","Oh I wish I were an aardvark.",12)
	testCaseH(2, "were","Oh I wish I were an aardvark.",[12,7])
	#testCaseH(3, "join","Oh I wish I were an aardvark.",-1)
	testCaseH(3, "join","Oh I wish I were an aardvark.",[-1,7])
	#testCaseH(4, "seashore","She sells sea shells by the seashore.",28)
	testCaseH(4, "seashore","She sells sea shells by the seashore.",[28,14])
	testCaseH(5, "00001","0"*1000, [-1,996])
	testCaseH(6, "10000","0"*1000, [-1,4980])
	testCaseH(7, "01010","0"*1000, [-1,996])

def HorspoolMatching(pattern,text):
  count = 0
  table = shiftTable(pattern)
  ptr = len(pattern) - 1
  while ptr <= (len(text)-1):
    k = 0
    count += 1
    while (k <= len(pattern)-1) and (pattern[len(pattern)-1-k] == text[ptr-k]):
      k += 1
      count += 1
    if k == len(pattern):
      return [(ptr-len(pattern)+1),count-1]
    elif text[ptr] in table:
      ptr += table[text[ptr]]
    else:
      ptr += len(pattern)
  return [-1,count]

testHorspool()

############################
#PART E
############################

def stringMatch(fullText,findText):
  #initialize values to return
  result = -1
  counter = 0
  #set length variables
  fullLength, findLength = len(fullText), len(findText)
  #loop through fullText
  for index in range(0, fullLength - findLength + 1):
    stepper = 0
    #increment counter to account for comparisons that resolve to false
    counter += 1
    #increment stepper until either the word is found or the current sequence of letters cannot be the word
    while stepper < findLength and findText[stepper] == fullText[index + stepper]:
      #increment counter to account for all comparisons during time spent in the while loop
      counter += 1
      stepper += 1
    #if the full desired word is found, return its index and break the for loop
    if stepper == findLength:
      #remove one from counter because, if the word is found, the first letter will have been counted twice 
      counter -= 1
      result = index
      break
  return [result,counter]
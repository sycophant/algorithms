#####################
#Name : Arif Hasnat
#Student ID: 482131
#Class: Algortihms
#Date: 05/09/19
#Professor: Browning
#####################

#####################
#Part A and B
#####################

#test method for grain count
def testCount():
	#basic test
	if numberOfGrains(lambda k:k,10)==5050:
		print ("Test 1 PASS.")
	else:
		print ("Test 1 FAIL")
	#square progression test
	if numberOfGrains(lambda k:k*k,2)==30:
		print ("Test 2 PASS.")
	else:
		print ("Test 2 FAIL")
	#basic test for 8-sided board with doubling function
	if numberOfGrains(lambda k:2**(k+1)-1,8)==73786976294838206396:
		print ("Test 3 PASS.")
	else:
		print ("Test 3 FAIL")
	#test for board where only 2 grains are added
	if numberOfGrains(lambda k:k+2,8)==2208:
		print ("Test 4 PASS.")
	else:
		print ("Test 4 FAIL")

#test method for time
def testTime():
	#test for time taken for basic test
	if timeToCount(lambda k:k,10)==[10,24,1,0,0]:
		print ("Test 1 PASS.")
	else:
		print ("Test 1 FAIL")
	#test for time taken for square test
	if timeToCount(lambda k:k*k,10)==[10,59,21,3,0]:
		print ("Test 2 PASS.")
	else:
		print ("Test 2 FAIL")
	#test for time taken for 8-sided board filled with doubling function
	if timeToCount(lambda k:2**(k+1)-1,8)==[56, 59, 3, 105, 2339769669420]:
		print ("Test 3 PASS.")
	else:
		print ("Test 3 FAIL")
	#test for time taken for 8-sided board filled with two grains per square
	if timeToCount(lambda k:k+2,8)==[48, 36, 0, 0, 0]:
		print ("Test 4 PASS.")
	else:
		print ("Test 4 FAIL")

#function to couunt tumber of grains by takin in input as function and dimensions of board
#outputs number of grains that it needs to populate a board via the function used.
def numberOfGrains(myfun,dimension):
	count = dimension*dimension	#count of squares from dimension of board
	grains = 0
	for x in range(0, count):	#loop for concatenating multiple iterations of the formula given per square
		grains += myfun(x+1)
	return grains

#function takes in input a function and dimension of board, outputs time taken to do function
#given one second is taken to place one grain on the board.
def timeToCount(myfun,dimension):
	m, s = divmod(numberOfGrains(myfun,dimension), 60) #count of grains from function, to minutes
	h, m = divmod(m, 60)	#minute to hour
	d, h = divmod(h, 24)	#hour to day
	y, d = divmod(d, 365)	#day to year
	return [s,m,h,d,y]

#####################
#Part C
#####################

#Put in values in the timeToCount function till day counter was breached.
#20 for identity
#7 for square
#3 sided board.
#20 for 2 increment
#####################
#Name : Arif Hasnat
#Student ID: 482131
#Class: Algortihms
#Date: 05/09/19
#Professor: Browning
#####################

#####################
#PART A
#####################

def bfString(searchString, longText):	#takes substring and string
	diff = len(longText) - len(searchString) + 1 #sets up difference in length between strings
	for strIndex in range (0, diff):
		posText = 0
		while posText<len(searchString) and searchString[posText] == longText[strIndex+posText]:
			posText += 1
			if posText == len(searchString):
				return strIndex
	return -1

def testCaseA(testNumber,longText,searchString,expectedResult):
	actualResult = bfString(searchString, longText)
	if actualResult == expectedResult: print("Test",testNumber,"passed.")
	else: print("Test",testNumber,"failed.  Expected",expectedResult, "but found",actualResult)

def testA():
	testCaseA(1,"Oh I wish I were an aardvark.","were",12) 
	testCaseA(2,"Oh I wish I were an aardvark.","join",-1) 
	testCaseA(3,"Oh my baby boy","baby",6)
	testCaseA(4,"She sells sea shells by the seashore.","seashore",28)	#own test case
	testCaseA(5,"Twas but a lark","lark",11)	#own test case 2

# testA()
# Test 1 passed.
# Test 2 passed.
# Test 3 passed.
# Test 4 passed.
# Test 5 passed.

#####################
#PART B
#####################

def bfStringB(searchString, longText):
	diff = len(longText) - len(searchString) + 1
	counter = 0
	for strIndex in range (0, diff):
		posText = 0
		while posText<len(searchString) and searchString[posText] == longText[strIndex+posText]:
			counter += 1	#while loop is entered only when comparison is made
			posText += 1
			if posText == len(searchString):
				print("Position: ",strIndex," No. of comparisons: ",counter)
				return strIndex
		counter += 1	#second counter for every time comparison is done
	return -1

def testCaseB(testNumber,longText,searchString,expectedResult):
	actualResult = bfStringB(searchString, longText)
	if actualResult == expectedResult: print("Test",testNumber,"passed.")
	else: print("Test",testNumber,"failed.  Expected",expectedResult, "but found",actualResult)

def testB():
	testCaseB(1,"Oh I wish I were an aardvark.","were",12) 
	testCaseB(2,"Oh I wish I were an aardvark.","join",-1) 
	testCaseB(3,"Oh my baby boy","baby",6)
	testCaseB(4,"She sells sea shells by the seashore.","seashore",28)
	testCaseB(5,"Twas but a lark","lark",11)

# >>> testB()
# ('Position: ', 12, ' No. of comparisons: ', 5)
# ('Test', 1, 'passed.')
# ('Test', 2, 'passed.')
# ('Position: ', 6, ' No. of comparisons: ', 4)
# ('Test', 3, 'passed.')
# ('Position: ', 28, ' No. of comparisons: ', 16)
# ('Test', 4, 'passed.')
# ('Position: ', 11, ' No. of comparisons: ', 4)
# ('Test', 5, 'passed.')

#####################
#PART C
#####################

def testC():
	testCaseB(6,"SORTING_ALGORITHM_CAN_USE_BRUTE_FORCE_METHOD","SEARCH",-1)
	testCaseB(7,"AAAAAAB","AAB",4)
	testCaseB(8,"AAAAAAAAAAAAAAAAAAAB","AAAAB",15)
	testCaseB(9,"10000000","1",0)

# >>> testC()
# ('Test', 6, 'passed.')
# ('Position: ', 4, ' No. of comparisons: ', 15)
# ('Test', 7, 'passed.')
# ('Position: ', 15, ' No. of comparisons: ', 80)
# ('Test', 8, 'passed.')
# ('Position: ', 0, ' No. of comparisons: ', 1)
# ('Test', 9, 'passed.')

#for Test 6 the comparison count this algorithm returns is 42 compared
#to homework's 43. Faulty lacement of print statement after for loop decremented counter by 1.
#m(n-m+1)
#For tests 7 and 8, comparison counts are 15 and 80, matches with algorithm.
#Analysis matches as done in class, because same algorithm.
#Best case is when the algorithm does not know it has to find any specified string. So by formula,
#0(n-0+1) = 0(n-1) = 0
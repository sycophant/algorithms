####################################
#Lexicographic Permutations
#Authors: A. Hasnat, B. Guthrie, V. Mjimba
#Professor: Carol Browning
####################################

##################
#Card Questions
##################
# Given a list of letters ['a','a','b','b'], all permutations lexicographically.
# 	Start with aabb
# ['aabb', 'abab', 'abba', 'baab', 'baba', 'bbaa']

# Walk through the algorithm for n=4.
# [1, 2, 3, 4]
# [1, 2, 4, 3]
# [1, 3, 2, 4]
# [1, 3, 4, 2]
# [1, 4, 2, 3]
# [1, 4, 3, 2]
# [2, 1, 3, 4]
# [2, 1, 4, 3]
# [2, 3, 1, 4]
# [2, 3, 4, 1]
# [2, 4, 1, 3]
# [2, 4, 3, 1]
# [3, 1, 2, 4]
# [3, 1, 4, 2]
# [3, 2, 1, 4]
# [3, 2, 4, 1]
# [3, 4, 1, 2]
# [3, 4, 2, 1]
# [4, 1, 2, 3]
# [4, 1, 3, 2]
# [4, 2, 1, 3]
# [4, 2, 3, 1]
# [4, 3, 1, 2]
# [4, 3, 2, 1]

##################
#Part A
##################

#swaps two items in the list to create the next permutation
def swap(list, a, b):
	temp = list[a]
	list[a] = list[b]
	list[b] = temp
def call(nlist):
	length = len(nlist)
	#let i be its largest index such that a i < a i+1 //a i+1 > a i+2 > . . . > a n
	i = length - 2
	while (i >= 0 and nlist[i] >= nlist[i+1]): i -= 1
	#check for cleared all permutations
	if (i == -1): return False
	#find the largest index j such that a i < a j //j ≥ i + 1 since a i < a i+1
	j = i + 1
	while (j < length and nlist[j] > nlist[i]): j+=1
	j -= 1
	#swap a i with a j //a i+1 a i+2 . . . a n will remain in decreasing order
	swap(nlist, i, j)
	#reverse the order of the n from a i+1 to a n inclusive
	left = i + 1
	right = length - 1
	while (left < right):
		swap(nlist, left, right)
		left += 1
		right -= 1
	#check for permuted or not
	return True
#Wrapper function, prints permutations on numbers.
def permute(n):
	nlist = list(range(1, n+1))
    # while you can still do permutations continue calling
	while True:
		print(nlist)
        #Finsihed so ends
		if not call(nlist):
			break

##################
#Part B
##################

#Wrapper function, prints permutations on a given string.
def permuteList(string):
    output = []
    #while you can still do permuations keep calling to call(string)
    while True:
        output.append("".join(string))
        if not call(string):
            break
    return output
    
def testCase(testNumber, inputList,expectedResult):
	actualResult = permuteList(inputList)
	if actualResult == expectedResult: print('Test  ',testNumber,' passed.')
	else: print('Test  ',testNumber,' failed.' )
def test():
    testCase(1,['a','a','b','b'], ['aabb','abab','abba','baab','baba','bbaa'])
    testCase(2,['a','a','c','c'], ['aacc','acac','acca','caac','caca','ccaa'])
    testCase(3,['a','b'],['ab','ba'])
    testCase(4,['a','b','c','c'],['abcc', 'acbc', 'accb', 'bacc', 'bcac', 'bcca', 'cabc', 'cacb', 'cbac', 'cbca', 'ccab', 'ccba'])
    testCase(5,['a','b','b','c','c'],['abbcc', 'abcbc', 'abccb', 'acbbc', 'acbcb', 'accbb', 'babcc', 'bacbc', 'baccb', 'bbacc', 'bbcac', 'bbcca', 'bcabc', 'bcacb', 'bcbac', 'bcbca', 'bccab', 'bccba', 'cabbc', 'cabcb', 'cacbb', 'cbabc', 'cbacb', 'cbbac', 'cbbca', 'cbcab', 'cbcba', 'ccabb', 'ccbab', 'ccbba'])
    
# A transcript showing your test results goes here
#  test()
# Test   1  passed.
# Test   2  passed.
# Test   3  passed.
# Test   4  passed.
# Test   5  passed.